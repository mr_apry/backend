/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mrapry.backend.service.dao;

import com.mrapry.backend.service.Menu;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author mrapry
 */
public interface MenuService extends CrudRepository<Menu, String>{
    
    public List<Menu> findById(String id);
    public List<Menu> findByNama(String nama);
}
