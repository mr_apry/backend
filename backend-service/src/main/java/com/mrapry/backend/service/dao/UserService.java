/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mrapry.backend.service.dao;

import com.mrapry.backend.service.User;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author mrapry
 */
public interface UserService extends CrudRepository<User, String>{
    
    public List<User> findByUsername (String username);
    public List<User> findById (String id);
}
