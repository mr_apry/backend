/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mrapry.backend.web.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author mrapry
 */
@Controller
@RequestMapping(value = "/admin/role")
public class RoleController {
    @RequestMapping(value = "/")
    public String index(){
        return "/admin/role/list";
    }
    
    @RequestMapping(value = "/add")
    public void addMenu(){
        
    }
    
    @RequestMapping(value = "/edit")
    public void editMenu(){
        
    }
    
    @RequestMapping(value = "/del")
    public void del(){
        
    }
}
