/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mrapry.backend.web.admin;

import com.mrapry.backend.service.User;
import com.mrapry.backend.service.dao.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author mrapry
 */
@Controller
@RequestMapping(value = "/admin/user")
public class UserController {
    
    @Autowired
    private UserService userService;
    
    @RequestMapping(value = "/")
    public String index(ModelMap mm){
        mm.addAttribute("user",userService.findAll());
        return "/admin/user/list";
    }    
    
    @RequestMapping(value = "/add")
    public void addMenu(@RequestBody User user){
        userService.save(user);
    }
    
    @RequestMapping(value = "/edit",method = RequestMethod.POST)
    public void editMenu(@RequestBody String id, @RequestBody User user){
        User u = userService.findOne(id);
        if (u!=null) {
            user.setId(id);
            userService.save(user);
        } else {
            System.out.println("User tidak ditemukan");
        }
    }
    
    @RequestMapping(value = "/del",method = RequestMethod.POST)
    public void del(@RequestBody String id){
        User u = userService.findOne(id);
        if (u!=null) {
            userService.delete(u);
        } else {
            System.out.println("User tidak ditemukan");
        }
    }
}
