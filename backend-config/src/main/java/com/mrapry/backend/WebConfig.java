/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mrapry.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 *
 * @author mrapry
 */
@SpringBootApplication
@EnableAsync
@EnableAutoConfiguration
@ComponentScan({"com.mrapry"})
public class WebConfig extends SpringBootServletInitializer{
    public static void main(String[] args) {
        SpringApplication.run(WebConfig.class, args);
    }
    
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
         return builder.sources(WebConfig.class);
    }
}
